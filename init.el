(require 'package)
(add-to-list 'package-archives
         '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)


;;full screen buffer menu
(global-set-key (kbd "C-x C-b") 'buffer-menu)

;find other file
(global-set-key (kbd "C-x C-o") 'ff-find-other-file)

(setq ff-search-directories
      '("." "../src" "../include"))

;eat the new line when killing the line
(setq kill-whole-line t)

;fuck tabs
(setq-default indent-tabs-mode nil)

;default indentation 4
(setq-default c-basic-offset 4)

;default style
(setq c-default-style "BSD"
      c-basic-offset 4)

(global-set-key (kbd "<f4>") 'browse-url-at-point)
(global-set-key (kbd "<f5>") 'recompile)
(global-set-key (kbd "<f6>") 'compile)


(add-to-list 'load-path "~/.emacs.d/rust-mode/")
(autoload 'rust-mode "rust-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-mode))

(setq make-backup-files nil) ; stop creating backup~ files
(setq auto-save-default nil) ; stop creating #autosave# files


;;drag and drop file names into buffer rather than open them.
(setq dnd-protocol-alist '(("^file:" . dnd-insert-filename)))

(defun dnd-insert-filename (uri _action)
  (insert (file-relative-name (dnd-get-local-file-name uri))))


;;turn on auto complete
(require 'auto-complete)
(require 'auto-complete-config)
(ac-config-default)

;;flyspell mode for markdown
(add-hook 'markdown-mode-hook 'flyspell-mode)

;; no startup msg  
(setq inhibit-startup-message t)

;; stop that annoying behavior where I hit return and I selected an
;; auto-complete I didn't want
(define-key ac-completing-map [return] nil)
(define-key ac-completing-map "\r" nil)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["black" "#d55e00" "#009e73" "#f8ec59" "#0072b2" "#cc79a7" "#56b4e9" "white"])
 '(custom-enabled-themes (quote (deeper-blue))))


(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Source Code Pro" :foundry "outline" :slant normal :weight normal :height 98 :width normal)))))
